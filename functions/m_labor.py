import json

import h5py as h5


def logging_heater(path: str, uuid: str) -> dict[str, dict[str, float]]:
    """Prompt the user to enter heating data and log it to an HDF5 file.

    This function creates a new group in the HDF5 file for a specified UUID (heater's UUID)
    and prompts the user to enter current, voltage, and heating time. It validates the
    user's input and writes the data to the file under the created group. Each piece
    of data is stored in a separate dataset with a corresponding unit attribute.

    Args:
        path (str): The file path to the HDF5 file where data should be logged.
        uuid (str): The UUID to create a group for.

    Returns:
        dict[str, dict[str, float]]: A dictionary containing the entered data,
                                     organized by UUID and measurement type.

    Example:
        After executing this function, a new group named after the heater UUID is added to the target HDF5 file.
        H5-File-Name (+ Attributes: authors, created[time], experiment, groupNumber, testrig)
            RawData
                UUID1 (+ Attributes: name, serial)
                    temperature (this is a data set)
                    timestamp (this is a data set)
                UUID2 (+ Attributes: name, serial)
                    temperature (this is a data set)
                    timestamp (this is a data set)
                UUID3 (+ Attributes: name, serial)
                    temperature (this is a data set)
                    timestamp (this is a data set)
                ...
                UUID_Heater
                    current (+ Attribute: unit)
                    heat_time (+ Attribute: unit)
                    voltage (+ Attribute: unit)

    """
    # Define the HDF5 internal path for the heater data group.
    h5_path = "RawData/{}".format(uuid)

    # Open the HDF5 file and create a new group for the heater using the UUID.
    with h5.File(path, "a") as f:
        # TODO: Create Group in RawData group.
        g = f.create_group(h5_path)
        # DONE #

    # Initialize a dictionary to store heater data.
    data_dict = {
        uuid: {
            "Current in Ampere": None,
            "Voltage in Volt": None,
            "Heat time in Seconds": None,
        }
    }

    # A flag to indicate if the loop for correct data entry is active.
    is_wrong = True
    # A flag to indicate if the user is entering the data or correcting it.
    enter_data = True
    # Loop until correct data is entered.
    while is_wrong:
        # If entering data, prompt the user for each value.
        if enter_data:
            for i in data_dict[uuid]:
                try:
                    # Attempt to convert the input to float and store it.
                    data_dict[uuid][i] = float(input("{} = ".format(i)))
                except ValueError:
                    # If conversion fails, print an error message and set the value to None.
                    print("Invalid input, try again after entering the remaining data.")
                    data_dict[uuid][i] = None

            # Print the current state of data_dict for review.
            print()
            print(json.dumps(data_dict, indent=4))
        # Prompt the user to confirm if the entered data is correct.
        user_input = input(
            "Are the input data correct? [y] to store, [n] to re-enter: "
        )
        # Check the user's decision and set flags accordingly.
        if user_input == "y" or user_input == "Y":
            # If data is correct, exit the loop.
            is_wrong = False
        elif user_input == "n" or user_input == "N":
            # If data is incorrect, prompt for re-entry.
            enter_data = True
        else:
            # If input is invalid, inform the user and do not re-enter data.
            print("Invalid input")
            enter_data = False

    # Open the HDF5 file to write the data into the corresponding group.
    with h5.File(path, "r+") as f:
        # TODO: Saves the data entered by the user into the created group.
        # Current, voltage and time should be saved into separate data sets
        # with the attribute being corresponding unit.
        g = f[h5_path]
        c = g.create_dataset("current", data = data_dict[uuid]["Current in Ampere"])
        v = g.create_dataset("voltage", data = data_dict[uuid]["Voltage in Volt"])
        t = g.create_dataset("time", data = data_dict[uuid]["Heat time in Seconds"])
                             
        c.attrs["unit"] = "ampere"
        v.attrs["unit"] = "volt"
        t.attrs["unit"] = "second"

        # DONE #

    # Return the dictionary containing the logged data.
    return data_dict
