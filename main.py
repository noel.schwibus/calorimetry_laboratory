"""
Praktikum Digitalisierung
Script for the labor experiment.

Before you start, please read all the documentation carefully,
including the comments in the code and the docstring of the
functions. If you have any questions, please ask in the forum
or join the helpdesk!

Author:         Benjamin Hermann, M.Sc.
                https://git.rwth-aachen.de/benjamin.hermann
                Ning Xia, M.Sc. M.Sc.
                https://git.rwth-aachen.de/ning.xia
Created:        24.04.2023
Last Changes:   03.11.2023

"""
from functions import m_json, m_labor, m_pck

# Define the path to the datasheets folder.
path_json = "./datasheets"
# Define the path to the test rig setup file.
path_setup = "/home/pi/calorimetry_laboratory/datasheets/setup_PDC001.json"
# path_setup = ""

# Ask for the type of experiment to be performed until the user gives a valid answer.
is_log_heater = None
while is_log_heater != 0 and is_log_heater != 1:
    if is_log_heater is None:
        print("0 = measuring specific heat capacity of a probe")
        print("1 = measuring calorimeter constant")
        print()
    else:
        print("\nInvalid input\n")
        print("0 = measuring specific heat capacity of a probe")
        print("1 = measuring calorimeter constant")
        print()

    try:
        # Attempt to convert the input to int and store it.
        is_log_heater = int(input("measurement process = "))
    except ValueError:
        # If conversion fails, set value to 2, in order to ask user again.
        is_log_heater = 2

# Read the metadata necessary to control test rig from the setup file.
metadata = m_json.get_metadata_from_setup(path_setup)
# Read the sensor's serial number from the sensor's datasheets and add it to the dictionary that holds the metadata.
m_json.add_temperature_sensor_serials(path_json, metadata)
# Reads sensor measurements and saves the data to a variable.
data = m_pck.get_meas_data_calorimetry(metadata)

# List of valid characters.
valued_char = "-_.() abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
# Initialize empty filename.
name_logging = ""
# Loop until the user enters a valid filename.
while name_logging == "":
    # Read the name of the logging file to be created from stdin (standard input/output).
    name_logging = input("logging file name = ")
    # Remove spaces at the beginning and end of the filename.
    name_logging = name_logging.strip()
    # If the filename entered is empty then re-enter it.
    if name_logging == "":
        print("File name can not be empty.")
        continue
    # Iterate over all characters of the filename.
    for c in name_logging:
        # Requires retyping if invalid characters are present.
        if c not in valued_char:
            print("{} is not a valid name. ({} invalid)".format(name_logging, c))
            name_logging = ""
            break

# The folder containing the logging files will be created in the data folder in the root directory of the program.
path_logging = "{}/{}".format("data", name_logging)

# Save the datasets and datasheets used in the experiment to the logging directory.
m_pck.logging_calorimetry(data, metadata, path_logging, path_json)
m_json.archiv_json(path_json, path_setup, path_logging)

if is_log_heater:
    # Read heater's uuid from metadata.
    heater_uuid = metadata["actor"]["values"][
        metadata["actor"]["names"].index("immersion_heater")
    ]
    # Save heater-related data to an HDF5 file.
    data = m_labor.logging_heater(
        "{}/{}.h5".format(path_logging, name_logging), heater_uuid
    )
